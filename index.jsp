<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Payroll Calculator</title>
</head>
<body>
<form action="Calculate" method="get">
    <h1><%= "Payroll Calculator" %>
    </h1>
    <table width="350px" border="1">

        <tr>
            <td>Hours Worked</td>
            <td><input type="text" placeholder="Hours" id="hoursWorked" name="hoursWorked"> </td>
        </tr>

        <tr>
            <td>Hourly Wage</td>
            <td><input type="text" placeholder="Wage" id="hourlyWage" name="hourlyWage"> </td>
        </tr>

        <tr>
            <td>Federal Tax</td>
            <td><input  type="text" placeholder="Percentage" id="federalTax" name="federalTax"> % </td>
        </tr>

        <tr>
            <td>State Tax</td>
            <td><input  type="text" placeholder="Percentage" id="stateTax" name="stateTax"> % </td>
        </tr>

        <tr>
            <td colspan="2"><input type="submit" value="Calculate"> </td>
        </tr>

    </table>
</form>
</body>
</html>