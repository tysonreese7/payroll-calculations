//Tyson Reese
//July 14th 2021
package com.example.FinalProject;

import org.junit.Test;
import java.io.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.PrintWriter;

import static org.junit.Assert.assertTrue;

@WebServlet(name = "Calculate", value = "/Calculate")
public class Calculate extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        PrintWriter output = response.getWriter();

        double hoursWorked = Double.parseDouble(request.getParameter("hoursWorked"));
        double hourlyWage = Double.parseDouble(request.getParameter("hourlyWage"));
        double federalTax = Double.parseDouble(request.getParameter("federalTax"));
        double stateTax = Double.parseDouble(request.getParameter("stateTax"));

        double feddecimal = federalTax / 100;
        double statedecimal = stateTax / 100;

        double grossPay = hourlyWage * hoursWorked;
        double fedWithholding = grossPay * feddecimal;
        double stateWithholding = grossPay * statedecimal;
        double netPay = grossPay - fedWithholding - stateWithholding;

        output.print("Payroll Calculations" + "<br/>" );
        output.println("<br/>");
        output.print(" Gross Pay = $" + String.format("%4.2f", grossPay) + "<br/>");
        output.print(" Federal Withholding = $" + String.format("%4.2f", fedWithholding) + "<br/>");
        output.print(" State Withholding = $" + String.format("%4.2f", stateWithholding) + "<br/>");
        output.print(" Net Pay = $" + String.format("%4.2f", netPay) + "<br/>");

        class AssertionTest {
            @Test
            public void assertions() {
                assertTrue(hoursWorked >= 0);
                assertTrue(hourlyWage >= 0);
                assertTrue(federalTax >= 0);
                assertTrue(stateTax >= 0);
            }
        }
    }
}


